import net.liftweb.json.prettyRender
import org.json4s
import org.json4s.Xml.toJson
import org.json4s._
import org.json4s.native.JsonMethods._

import scala.xml.XML
import org.json4s.native.Serialization.write
implicit val jsonFormats = DefaultFormats
import java.util._


val myxml = <config xmlns="http://tail-f.com/ns/config/1.0">
  <orgs xmlns="http://www.versa-networks.com/org">
    <org-services>
      <name>comcast</name>
      <tdf xmlns="http://www.versa-networks.com/tdf">
        <subscribers>
          <extensive>
            <id>74:54:7d:7f:9d:e2</id>
            <report_count>35</report_count>
            <report_interval>3600</report_interval>
            <tcp-session>0</tcp-session>
            <udp-session>0</udp-session>
            <other-session>0</other-session>
            <high-priority-report-count>0</high-priority-report-count>
            <usage-report-user-data>0</usage-report-user-data>
            <usage-report-duration>3600</usage-report-duration>
            <usage-report-start-time>2019-05-02, 14:50:07</usage-report-start-time>
            <subscriber_create_time>2019-04-24, 13:25:20</subscriber_create_time>
            <subscriber_update_time>2019-05-02, 15:40:32</subscriber_update_time>
            <dhcp_v4_relay_agent>10.254.205.1</dhcp_v4_relay_agent>
            <dhcp_v6_relay_agent>2001:558:ff89:239::1</dhcp_v6_relay_agent>
            <cm_mac>74:54:7d:7f:9d:e2</cm_mac>
            <cm_ipv6_host>2001:558:ff89:239:7654:7dff:fe7f:9de2</cm_ipv6_host>
            <cm_ipv6_learnt_from>DHCP Broadcast Snoop</cm_ipv6_learnt_from>
            <cm_ip_update_time>2019-05-02, 13:28:53</cm_ip_update_time>
            <cm_lease_expiry>2019-05-03, 13:28:53</cm_lease_expiry>
            <mta_mac>74:54:7d:7f:9d:e3</mta_mac>
            <mta_ipv4_host>10.254.205.214</mta_ipv4_host>
            <mta_ipv4_learnt_from>DHCP Unicast Snoop</mta_ipv4_learnt_from>
            <mta_ip_update_time>2019-05-02, 15:40:32</mta_ip_update_time>
            <mta_lease_expiry>2019-05-02, 16:00:32</mta_lease_expiry>
            <cpe_info>
              <cpe_mac>74:54:7d:7f:9d:e4</cpe_mac>
              <cpe_ipv4_host>10.254.206.85</cpe_ipv4_host>
              <cpe_ipv4_learnt_from>DHCP Unicast Snoop</cpe_ipv4_learnt_from>
              <cpe_ipv4_update_time>2019-05-02, 15:21:13</cpe_ipv4_update_time>
              <cpe_ipv4_lease_expiry>2019-05-02, 16:21:13</cpe_ipv4_lease_expiry>
              <cpe_ipv6_host>2001:558:ff89:a58e:e8ce:fc15:30c7:317b</cpe_ipv6_host>
              <cpe_ipv6_learnt_from>DHCP Broadcast Snoop</cpe_ipv6_learnt_from>
              <cpe_ipv6_update_time>2019-05-02, 15:25:31</cpe_ipv6_update_time>
              <cpe_ipv6_lease_expiry>2019-05-02, 16:25:31</cpe_ipv6_lease_expiry>
              <cpe_ipv6_prefix>2001:558:ff89:b995::</cpe_ipv6_prefix>
              <cpe_ipv6_prefix_len>64</cpe_ipv6_prefix_len>
              <cpe_ipv6_prefix_learnt_from>DHCP Broadcast Snoop</cpe_ipv6_prefix_learnt_from>
              <cpe_ipv6_prefix_update_time>2019-05-02, 15:38:44</cpe_ipv6_prefix_update_time>
              <cpe_ipv6_prefix_lease_expiry>2019-05-02, 16:38:33</cpe_ipv6_prefix_lease_expiry>
            </cpe_info>
            <rule-info>
              <name>r_dscp_9</name>
              <uplink_ipv4_bytes>0</uplink_ipv4_bytes>
              <downlink_ipv4_bytes>0</downlink_ipv4_bytes>
              <uplink_ipv6_bytes>0</uplink_ipv6_bytes>
              <downlink_ipv6_bytes>0</downlink_ipv6_bytes>
              <uplink_ipv4_bytes_prev>0</uplink_ipv4_bytes_prev>
              <downlink_ipv4_bytes_prev>0</downlink_ipv4_bytes_prev>
              <upLink_ipv6_bytes_prev>0</upLink_ipv6_bytes_prev>
              <downlink_ipv6_bytes_prev>0</downlink_ipv6_bytes_prev>
            </rule-info>
            <rule-info>
              <name>r_dscp_8</name>
              <uplink_ipv4_bytes>0</uplink_ipv4_bytes>
              <downlink_ipv4_bytes>0</downlink_ipv4_bytes>
              <uplink_ipv6_bytes>0</uplink_ipv6_bytes>
              <downlink_ipv6_bytes>0</downlink_ipv6_bytes>
              <uplink_ipv4_bytes_prev>0</uplink_ipv4_bytes_prev>
              <downlink_ipv4_bytes_prev>0</downlink_ipv4_bytes_prev>
              <upLink_ipv6_bytes_prev>0</upLink_ipv6_bytes_prev>
              <downlink_ipv6_bytes_prev>0</downlink_ipv6_bytes_prev>
            </rule-info>
            <rule-info>
              <name>r_dscp_7</name>
              <uplink_ipv4_bytes>0</uplink_ipv4_bytes>
              <downlink_ipv4_bytes>0</downlink_ipv4_bytes>
              <uplink_ipv6_bytes>0</uplink_ipv6_bytes>
              <downlink_ipv6_bytes>0</downlink_ipv6_bytes>
              <uplink_ipv4_bytes_prev>0</uplink_ipv4_bytes_prev>
              <downlink_ipv4_bytes_prev>0</downlink_ipv4_bytes_prev>
              <upLink_ipv6_bytes_prev>0</upLink_ipv6_bytes_prev>
              <downlink_ipv6_bytes_prev>0</downlink_ipv6_bytes_prev>
            </rule-info>
            <rule-info>
              <name>r_dscp_6</name>
              <uplink_ipv4_bytes>0</uplink_ipv4_bytes>
              <downlink_ipv4_bytes>0</downlink_ipv4_bytes>
              <uplink_ipv6_bytes>26340</uplink_ipv6_bytes>
              <downlink_ipv6_bytes>22180</downlink_ipv6_bytes>
              <uplink_ipv4_bytes_prev>0</uplink_ipv4_bytes_prev>
              <downlink_ipv4_bytes_prev>0</downlink_ipv4_bytes_prev>
              <upLink_ipv6_bytes_prev>46522</upLink_ipv6_bytes_prev>
              <downlink_ipv6_bytes_prev>51454</downlink_ipv6_bytes_prev>
            </rule-info>
            <rule-info>
              <name>r_dscp_5</name>
              <uplink_ipv4_bytes>0</uplink_ipv4_bytes>
              <downlink_ipv4_bytes>0</downlink_ipv4_bytes>
              <uplink_ipv6_bytes>0</uplink_ipv6_bytes>
              <downlink_ipv6_bytes>0</downlink_ipv6_bytes>
              <uplink_ipv4_bytes_prev>0</uplink_ipv4_bytes_prev>
              <downlink_ipv4_bytes_prev>0</downlink_ipv4_bytes_prev>
              <upLink_ipv6_bytes_prev>0</upLink_ipv6_bytes_prev>
              <downlink_ipv6_bytes_prev>0</downlink_ipv6_bytes_prev>
            </rule-info>
            <rule-info>
              <name>r_dscp_4</name>
              <uplink_ipv4_bytes>0</uplink_ipv4_bytes>
              <downlink_ipv4_bytes>0</downlink_ipv4_bytes>
              <uplink_ipv6_bytes>0</uplink_ipv6_bytes>
              <downlink_ipv6_bytes>0</downlink_ipv6_bytes>
              <uplink_ipv4_bytes_prev>0</uplink_ipv4_bytes_prev>
              <downlink_ipv4_bytes_prev>0</downlink_ipv4_bytes_prev>
              <upLink_ipv6_bytes_prev>0</upLink_ipv6_bytes_prev>
              <downlink_ipv6_bytes_prev>0</downlink_ipv6_bytes_prev>
            </rule-info>
            <rule-info>
              <name>r_dscp_3</name>
              <uplink_ipv4_bytes>10804</uplink_ipv4_bytes>
              <downlink_ipv4_bytes>0</downlink_ipv4_bytes>
              <uplink_ipv6_bytes>6192</uplink_ipv6_bytes>
              <downlink_ipv6_bytes>0</downlink_ipv6_bytes>
              <uplink_ipv4_bytes_prev>23712</uplink_ipv4_bytes_prev>
              <downlink_ipv4_bytes_prev>0</downlink_ipv4_bytes_prev>
              <upLink_ipv6_bytes_prev>15295</upLink_ipv6_bytes_prev>
              <downlink_ipv6_bytes_prev>0</downlink_ipv6_bytes_prev>
            </rule-info>
            <rule-info>
              <name>r_dscp_2</name>
              <uplink_ipv4_bytes>0</uplink_ipv4_bytes>
              <downlink_ipv4_bytes>2380</downlink_ipv4_bytes>
              <uplink_ipv6_bytes>0</uplink_ipv6_bytes>
              <downlink_ipv6_bytes>10808</downlink_ipv6_bytes>
              <uplink_ipv4_bytes_prev>0</uplink_ipv4_bytes_prev>
              <downlink_ipv4_bytes_prev>2856</downlink_ipv4_bytes_prev>
              <upLink_ipv6_bytes_prev>0</upLink_ipv6_bytes_prev>
              <downlink_ipv6_bytes_prev>25151</downlink_ipv6_bytes_prev>
            </rule-info>
          </extensive>
        </subscribers>
      </tdf>
    </org-services>
  </orgs>
</config>
val xml_json = toJson(XML.loadString(myxml.toString()))

def subXmlToJson(xml: String): JValue = {
  lazy val fmt = "yyyy-MM-dd', 'HH:mm:ss"
//  logger info s"subXmlToJson - \n$xml"
  (toJson(XML.loadString(xml)) \\ "extensive").transformField {
    case ("report_count", JString(s)) => ("report_count", JInt(s.toInt))
    case ("report_interval", JString(s)) => ("report_interval", JInt(s.toInt))
    case ("tcp-session", JString(s)) => ("tcp_session", JInt(s.toInt))
    case ("udp-session", JString(s)) => ("udp_session", JInt(s.toInt))
    case ("other-session", JString(s)) => ("other_session", JInt(s.toInt))
    case ("high-priority-report-count", JString(s)) => ("high_priority_report_count", JInt(s.toInt))
    case ("usage-report-user-data", JString(s)) => ("usage_report_user_data", JInt(s.toInt))
    case ("usage-report-duration", JString(s)) => ("usage_report_duration", JInt(s.toInt))
    case ("usage-report-start-time", JString(s)) => ("usage_report_start_time", JString(s))
    case ("subscriber_create_time", JString(s)) => ("subscriber_create_time", JString(s))
    case ("subscriber_update_time", JString(s)) => ("subscriber_update_time", JString(s))
    case ("cm_ip_update_time", JString(s)) => ("cm_ip_update_time", JString(s))
    case ("cm_lease_expiry", JString(s)) => ("cm_lease_expiry", JString(s))

    case ("mta_ip_update_time", JString(s)) => ("mta_ip_update_time", JString(s))
    case ("mta_lease_expiry", JString(s)) => ("mta_lease_expiry", JString(s))
    case ("cpe_ipv4_update_time", JString(s)) => ("cpe_ipv4_update_time", JString(s))
    case ("cpe_ipv4_lease_expiry", JString(s)) => ("cpe_ipv4_lease_expiry", JString(s))
    case ("cpe_ipv6_update_time", JString(s)) => ("cpe_ipv6_update_time", JString(s))
    case ("cpe_ipv6_lease_expiry", JString(s)) => ("cpe_ipv6_lease_expiry", JString(s))
    case ("cpe_ipv6_prefix_len", JString(s)) => ("cpe_ipv6_prefix_len", JInt(s.toInt))
    case ("cpe_ipv6_prefix_update_time", JString(s)) => ("cpe_ipv6_prefix_update_time", JString(s))
    case ("cpe_ipv6_prefix_lease_expiry", JString(s)) => ("cpe_ipv6_prefix_lease_expiry", JString(s))

    case ("uplink_ipv4_bytes", JString(s)) => ("uplink_ipv4_bytes", JLong(s.toLong))
    case ("downlink_ipv4_bytes", JString(s)) => ("downlink_ipv4_bytes", JLong(s.toLong))
    case ("uplink_ipv6_bytes", JString(s)) => ("uplink_ipv6_bytes", JLong(s.toLong))
    case ("downlink_ipv6_bytes", JString(s)) => ("downlink_ipv6_bytes", JLong(s.toLong))
    case ("uplink_ipv4_bytes_prev", JString(s)) => ("uplink_ipv4_bytes_prev", JLong(s.toLong))
    case ("downlink_ipv4_bytes_prev", JString(s)) => ("downlink_ipv4_bytes_prev", JLong(s.toLong))
    case ("upLink_ipv6_bytes_prev", JString(s)) => ("upLink_ipv6_bytes_prev", JLong(s.toLong))
    case ("downlink_ipv6_bytes_prev", JString(s)) => ("downlink_ipv6_bytes_prev", JLong(s.toLong))
    case ("cpe_info", JArray(s))=>("cpe_info______" , JArray(s))
    case ("cpe_info", s:JObject)=>("cpe_info______", JArray(s::Nil))
    case ("rule-info", JObject(s)) => ("rule_info", JObject(s))

  }
}



val test = xml_json.transformField{
  case (n, v) => (n.toUpperCase, v)
}
 val test2 = subXmlToJson(myxml.toString())

import java.io._
val pw = new PrintWriter(new File("/home/nandigamchandu/Desktop/myxml/src/main/scala/sample3.json" ))
pw.write(write(test2))
pw.close()
parse(""" { "numbers" : [10]} """)
val x = parse(""" { "numbers" : "10" } """)
val test4 = parse(""" { "age" : "10" ,  "name": "chandu"} """)

//val test5 = test4.transform{
//  case obj:JObject => obj.transformField{
//    case JField("age", JString(s)) => JField("age", JArray(List(JInt(s.toInt))))
//  }
//}

JObject(List(("fsfb",