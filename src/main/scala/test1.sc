import org.json4s._
import org.json4s.jackson.JsonMethods._

import scala.xml.XML


val xml =
  <users>
    <user>
      <id>1</id>
      <name>Harry</name>
    </user>
    <user>
      <id>2</id>
      <name>David</name>
    </user>
  </users>

XML.loadString(xml.toString())

parse(""" { "age" : [10] } """)