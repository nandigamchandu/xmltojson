name := "myxml"

version := "0.1"

scalaVersion := "2.12.8"

// https://mvnrepository.com/artifact/net.liftweb/lift-json
libraryDependencies += "net.liftweb" %% "lift-json" % "3.2.0"
//libraryDependencies += "org.json4s" % "json4s-core_2.12" % "3.6.5"
//libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.2.0"
// https://mvnrepository.com/artifact/org.json4s/json4s-ast
//libraryDependencies += "org.json4s" %% "json4s-ast" % "3.6.5"
// https://mvnrepository.com/artifact/org.json4s/json4s-jackson
//libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.6.5"

libraryDependencies += "org.json4s" %% "json4s-native" % "3.5.4"
libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.5.4"


